﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TrainingProject1.Framework.Data;

namespace TrainingProject1.Framework.UI
{
	public class BookUI : MonoBehaviour {
		
		[SerializeField]private List<PageUI> pageUI;
		[SerializeField]private Text titleUI;


		public void InitBookUI( Book mybook)
		{
			titleUI.text = mybook.Title;
			pageUI = new List<PageUI> ();

		}

	}
}
