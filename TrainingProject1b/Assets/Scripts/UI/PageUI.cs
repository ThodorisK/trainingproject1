﻿using UnityEngine;
using UnityEngine.UI;
using TrainingProject1.Framework.Data;

namespace TrainingProject1.Framework.UI
{
	public class PageUI : MonoBehaviour {

		[SerializeField] Text pageTitleUI;
		[SerializeField] Text pageTextUI;
		[SerializeField] Text pageNumberUI;

		public void InitPageUi(Book mybook, int i)
		{
			pageTitleUI.text = mybook.Pages[i].Title;
			pageTextUI.text = mybook.Pages[i].Text;
			pageNumberUI.text= (i+1)+"/"+mybook.Pages.Count;

		}

	}
}