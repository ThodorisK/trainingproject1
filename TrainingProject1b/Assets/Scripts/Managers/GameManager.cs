﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrainingProject1.Framework.Managers{

	public class GameManager : MonoBehaviour {


		public static GameManager instance = null;

		void Awake()
		{
			if (instance==null) {
				instance = this;
			}
			else if (instance !=this) {
				Destroy (gameObject);
			}


		}
	}
}