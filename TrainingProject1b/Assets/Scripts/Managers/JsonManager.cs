﻿using System.Collections;
using System.Collections.Generic;
using FullSerializer;
using TrainingProject1.Framework.Data;

namespace TrainingProject1.Framework.Managers
{
	public class JsonManager
	{
		private static readonly fsSerializer _serializer = new fsSerializer();

		public static Book getBook(string jsontext)
		{
			fsData data = fsJsonParser.Parse (jsontext);

			Book deserialized = null;
			_serializer.TryDeserialize<Book>(data, ref deserialized).AssertSuccessWithoutWarnings ();

			return deserialized;

		}

	}
}
