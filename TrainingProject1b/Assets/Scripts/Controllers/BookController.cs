﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrainingProject1.Framework.Managers;
using TrainingProject1.Framework.Data;

namespace TrainingProject1.Framework.Controllers
{
	public class BookController : MonoBehaviour{


		public static string LoadResourceTextfile(string filename)
		{
			TextAsset targetFile = Resources.Load<TextAsset>(filename);
			return targetFile.text;
            
		}
			
	}
}