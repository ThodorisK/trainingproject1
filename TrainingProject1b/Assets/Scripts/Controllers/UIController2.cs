﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TrainingProject1.Framework.Data;
using TrainingProject1.Framework.Managers;
using TrainingProject1.Framework.UI;


namespace TrainingProject1.Framework.Controllers
{
	public class UIController2 : MonoBehaviour {


		const string filename = "book-simple";

		private Book mybook;
		private int index;

		public BookUI bookUI;
		public PageUI pageUI;



		void Awake()
		{


			string jsontext = BookController.LoadResourceTextfile (filename);
			mybook = JsonManager.getBook (jsontext);

			bookUI.InitBookUI (mybook);
			pageUI.InitPageUi (mybook, 0);

			index =1;
		}

		void Update()
		{

			if (Input.GetKeyUp(KeyCode.LeftArrow) && index!=1){

				index--;
				pageUI.InitPageUi (mybook, index-1);				

			}

			if (Input.GetKeyUp(KeyCode.RightArrow) && index!=mybook.Pages.Count){

				index++;
				pageUI.InitPageUi (mybook, index-1);

			}
				

		}
	}
}