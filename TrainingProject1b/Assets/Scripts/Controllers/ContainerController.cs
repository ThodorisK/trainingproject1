﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrainingProject1.Framework.Data;
using TrainingProject1.Framework.Managers;

namespace TrainingProject1.Framework.Controllers
{
    public class ContainerController : MonoBehaviour
    {
        public float speed = 500f;
        
        void Update()
        {
            
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.Translate(Vector3.right * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    transform.Translate(Vector3.left * speed * Time.deltaTime);
                }
        }
    }
}