﻿using System.Collections;
using System.Collections.Generic;

namespace TrainingProject1.Framework.Data
{
	[System.Serializable]
	public class Page
	{
		public string Title{ get; set; }
		public string Text{get;set;}

		//Constructor
		public Page(){}
	}
}
