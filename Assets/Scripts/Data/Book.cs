﻿using System.Collections;
using System.Collections.Generic;

namespace TrainingProject1.Framework.Data
{
	[System.Serializable]
	public class Book
	{
		public List<Page> Pages { get; set; }
		public string Title {get; set;}

		public Book(){}
	}
}
