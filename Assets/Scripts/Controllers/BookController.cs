﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TrainingProject1.Framework.Managers;
using TrainingProject1.Framework.Data;

namespace TrainingProject1.Framework.Controllers
{
	public class BookController : MonoBehaviour {

		private const string filename = "book-simple";
		private Book mybook;

		
		void Awake()
		{
			string jsontext = LoadResourceTextfile (filename);
			mybook = JsonManager.getBook(jsontext);

			Debug.Log ("everything good");
		}

		public static string LoadResourceTextfile(string filename)
		{
			TextAsset targetFile = Resources.Load<TextAsset>(filename);
			return targetFile.text;

		}
	}
}